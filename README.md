# Dogu RBS


ROS에서 사운드 파일 재생을 도와주는 노드


## 파일 준비


원하는 디렉터리에 sound 폴더 생성 후,
sound 폴더 내부에 list.json과 sound 폴더를 생성하고,
두 번째 sound 폴더 내부에 재생할 wav 파일 저장.


### list.json


사운드 파일의 별명을 지정해주는 파일

```
{
	"사용할 별명": "실제 파일 이름",
	
	"startup": "desktop-login.wav",
	"shutdown": "xylofon.wav"
}
```

- 특수한 별명
  - startup: 컴퓨터 시작 시 재생할 사운드
  - shutdown: 컴퓨터 종료 시 재생할 사운드


## 노드 설명


- rbs_main: 주로 관리 역할 수행
- rbs_sub: 실질적으로 사운드 재생


**start.launch 파일로 실행**


### start.launch


- 파라미터
  - work_dir: 파일을 준비했던 디렉터리 (변수: directory)


## Catkin make


```
cd ~/catkin_ws
catkin_make_isolated --source src/dogu_rbs
```


## Auto Launch Setting


```
cd dogu_rbs
bash setup/autorun.sh
```


## 메시지 형태


- PlaySound.srv
  - 요청
    - string name: 사운드 이름
    - int64 loop: 사운드 반복 재생 횟수
    - int64 delay: 사운드 재생 간격 (재생 중에는 측정되지 않음)
  - 응답
    - (없음)


- ReloadList.srv
  - 요청
    - (없음)
  - 응답
    - string list: JSON 파일에서 읽어온 내용


- PowerSound.srv
  - 요청
    - bool startup: 컴퓨터 시작 사운드 활성 여부
    - bool shutdown: 컴퓨터 종료 사운드 활성 여부
  - 응답
    - (없음)


- SelectOutput.srv
  - 요청
    - string plug_name: 출력 장치 이름
  - 응답
    - bool result: 결과 (성공 - 빈 문자열, 실패 - 장치 목록)


## 서비스 설명


- rbs_main\_play
  - PlaySound.srv 사용
  - 사운드 파일 재생


- rbs_main\_reload
  - ReloadList.srv 사용
  - JSON 파일 다시 로드


- rbs_main\_powersound
  - PowerSound.srv 사용
  - 컴퓨터 시작, 종료 사운드 활성, 비활성


- rbs_main\_output
  - SelectOutput.srv 사용
  - 원하는 출력 장치 선택


## 사용


### 준비


만약 컴퓨터 실행 후 자동으로 실행되기 원한다면 setup/setup.sh을 실행한다.


### 실행


start.launch 파일을 통해 실행한다.

launch 파일 내의 directory 변수로 work_dir 파라미터를 설정할 수 있다.   
sound 폴더가 있는 디렉터리 주소를 넣어 실행한다.


### Service - /rbs_main\_output


처음 실행했다면 재생 장치를 선택해줘야 한다.

잘못된 장치 이름을 입력할 시 재생 장치 목록을 반환하는데,   
이를 이용해서 장치 목록을 확인할 수 있다.

장치 이름을 입력할 때 대소문자를 구분하지 않는다.   
또한 이름 전체를 입력하지 않아도 된다.   
예를 들어, 장치 이름이 'USB Speaker' 라면 'usb' 라고 선택해도 된다.

그런데 만약, 장치가 \['HDMI 0', 'HDMI 1'] 일때 'hdmi' 라고 입력한다면   
2개 이상 중복되기 때문에 선택되지 않는다.

프로그램이 실행되면 작업 디렉토리에 config.yaml 파일이 생성되는데,   
이 파일에 재생 장치 목록과 선택된 재생 장치가 저장되고,   
다음에 프로그램을 실행할 때 해당 설정이 반영된다.

따라서 프로그램 실행 중에는 서비스로, 아닐 때는 config.yaml 파일로 재생 장치를 선택할 수 있다.


### Service - /rbs_main\_play


사운드를 재생하려면 /rbs_main\_play 서비스를 이용하면 된다.

사운드 이름은 JSON 파일에서 설정한 별명과 sound 폴더 내 실제 파일 이름 모두 사용할 수 있다.

반복 횟수를 지정해줄 때 양수를 입력하면 숫자 만큼 반복되지만,   
0을 입력할 시 (재생되고 있더라도) 즉시 취소되고,   
음수를 입력할 시 무한 반복 하게 된다.

반복 재생 시 딜레이를 초 단위로 넣어줄 수 있는데,   
재생이 끝난 뒤에 딜레이가 들어간다.   
예를 들어, 5초 길이의 사운드에 2초의 딜레이를 넣었다면   
5초동안 재생된 후 2초 뒤에 다시 재생되게 되므로, 7초가 소요된다.

서비스를 요청할 때 현재 재생 장치도 같이 저장되기 때문에
이후 재생 장치가 변경된다고 해서 변경사항이 반영되지는 않는다.
예를 들어 'HDMI 0'으로 a.wav를 10번 재생한다고 했을 때,
3번 반복되었을 때 재생 장치를 'USB Speaker'로 변경해도 a.wav는 'HDMI 0'으로 재생된다.   
재생 장치 변경사항을 반영하려면 서비스를 다시 요청해야 한다.


### Service - /rbs_main\_reload


프로그램이 실행될 때 JSON 파일의 내용을 한번 읽어오기 때문에
실행된 뒤에 JSON 파일의 내용이 바뀌어도 프로그램에는 반영되지 않는다.   
변경사항이 생겼을 때 프로그램에도 업데이트 해주려면   
/rbs_main\_reload 서비스를 이용해야 한다.

서비스 응답으로 JSON 파일에서 읽은 내용을 반환한다.


### Service - /rbs_main\_powersound


컴퓨터 전원을 키고 끌 때에 사운드가 재생되게 설정할 수 있다.

/rbs_main\_powersound 서비스로 전원을 키고 끌 때의 사운드를 각각 키고 끌 수 있는데,   
활성화 시 서비스 파일과 스크립트 파일이 생성되고, 비활성화 시 삭제된다.
이 때, 사운드 파일은 JSON 파일에서 'startup', 'shutdown' 별명을 가진 사운드가 사용되고,
현재 재생 장치 설정이 반영된다.

만약 활성화 후 JSON 파일의 내용이나 재생 장치 설정이 변해 스크립트 파일은 변경되지 않는다.   
활성화 상태에서 다시 활성화 시키면 파일이 수정되지 않기 때문에
변경사항을 적용하려면 반드시 비활성화한 후 다시 활성화 해줘야 한다.
