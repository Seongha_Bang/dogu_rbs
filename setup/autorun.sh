echo "[Setup Dogu RBS]"

# Copy script file
echo "[Copy start script file]"
bash -c "sudo cp services/sound-start.sh /home/"
echo "[Finish copy script file]"

# Copy service script file
echo "[Copy service file]"
bash -c "sudo cp services/dogu-sound.service /etc/systemd/system/"
echo "[Finish copy service file]"

# Enable service
bash -c "sudo systemctl enable dogu-sound.service"

bash -c "sudo systemctl start dogu-sound.service"
echo "[Finish setup]"