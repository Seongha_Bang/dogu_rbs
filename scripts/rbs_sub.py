#!/usr/bin/env python

import rospy
import os
from threading import Thread
from time import sleep
from os import system

# from SoundLoopData import SoundLoopData

now_play = None


def sound_play(name, plug):
    global now_play

    system("aplay -D {} {} >/dev/null 2>&1".format(plug, name))

    sleep(1)
    now_play = None


def rbs_sub():
    global now_play

    rospy.init_node('rbs_sub', anonymous=True)
    loop = rospy.Rate(1)
    while not rospy.is_shutdown():
        try:
            sound_list = rospy.get_param('rbs_loop_data_list')

            for name in sound_list.keys():
                if sound_list[name]['loop_now'] >= sound_list[name]['loop'] >= 0:
                    if name == now_play and sound_list[name]['loop'] == 0:
                        os.system("killall -9 aplay >/dev/null 2>&1");
                        print('[Stop] {} (Forced)'.format(name));
                    else:
                        print('[Stop] {} (General)'.format(name));

                    del sound_list[name]
                else:
                    if sound_list[name]['delay_now'] >= sound_list[name]['delay'] and now_play == None:
                        now_play = name
                        print('[Play] {} ({}/{}) - {}'.format(name, sound_list[name]['loop_now'] + 1, sound_list[name]['loop'], sound_list[name]['plug']))
                        t = Thread(target=sound_play, args=(name, sound_list[name]['plug'],))
                        t.setDaemon(True)
                        t.start()

                        sound_list[name]['delay_now'] = 0
                        sound_list[name]['loop_now'] += 1

                    if now_play != name:
                        sound_list[name]['delay_now'] += 1

            rospy.set_param('rbs_loop_data_list', sound_list)
        except KeyError:
            pass

        loop.sleep()


if __name__ == '__main__':
    try:
        rbs_sub()
    except rospy.ROSInterruptException:
        pass
