#!/usr/bin/env python

import rospy
from dogu_rbs import *
from dogu_rbs.srv import *
import os
import json


class RBSMain:
    global work_dir

    def __init__(self):
        self.reload_file_list(None)
        self.ps = PowerSoundManage(work_dir, 'dogu_rbs_', 'st', 'sd')
        self.sd = SoundDevice(work_dir)
        self.plug = self.sd.config['select']

    def reload_file_list(self, req):
        work_dir = rospy.get_param('work_dir')
        self.data = dict()

        json_dir = work_dir + 'sound/list.json'

        try:
            with open(json_dir, 'r') as json_dir:
                self.data = json.load(json_dir)
        except IOError:
            rospy.logerr('Can\'t find json file - {}'.format(json_dir))

        result = self.data

        return ReloadListResponse(str(result))

    def play_sound(self, req):
        sound_list = rospy.get_param('rbs_loop_data_list')

        if req.name == '' and req.loop == 0:
            for name in sound_list.keys():
                sound_list[name]['loop'] = 0
        else:
            try:
                sound_name = '{}sound/sound/{}'.format(work_dir, self.data[req.name])
            except KeyError:
                sound_name = '{}sound/sound/{}'.format(work_dir, req.name)

            if os.path.isfile(sound_name):
                sound_list[sound_name] = SoundLoopData(req.loop, req.delay, self.plug)
            else:
                print("Can't find file - {}".format(sound_name))

        rospy.set_param('rbs_loop_data_list', sound_list)

        return PlaySoundResponse()

    def powersound_state(self, req):
        if req.startup:
            if 'startup' in self.data:
                if not self.ps.CheckStartupFile():
                    self.ps.CreateStartupService('{}sound/sound/{}'.format(work_dir, self.data['startup']), self.plug)
                self.ps.ChangeStartupServiceStatus('enable')
            else:
                rospy.logerr('Startup sound not specified')
        else:
            self.ps.ChangeStartupServiceStatus('disable')
            self.ps.RemoveStartupService()

        if req.shutdown:
            if 'shutdown' in self.data:
                if not self.ps.CheckShutdownFile():
                    self.ps.CreateShutdownService('{}sound/sound/{}'.format(work_dir, self.data['shutdown']), self.plug)
                self.ps.ChangeShutdownServiceStatus('enable')
                self.ps.ChangeShutdownServiceStatus('start')
            else:
                rospy.logerr('Shutdown sound not specified')
        else:
            self.ps.ChangeShutdownServiceStatus('disable')
            self.ps.ChangeShutdownServiceStatus('stop')
            self.ps.RemoveShutdownService()

        # if req.startup or req.shutdown:
        # 	if not self.ps.CheckPlaysoundModule() and self.ps.script_type == '.py':
        # 		self.ps.CreatePlaysoundModule()
        # else:
        # 	self.ps.RemovePlaysoundModule()

        os.system('sudo systemctl daemon-reload')
        return PowerSoundResponse()

    def select_output(self, req):
        plug = self.sd.GetDevicePlug(req.plug_name)
        response_str = ''

        if plug[0]:
            self.plug = plug[1]
            self.sd.UpdateConfigFile(work_dir, req.plug_name)
        else:
            rospy.logerr(plug[1])
            response_str = plug[1]

        return SelectOutputResponse(response_str)


def rbs_main():
    rbs = RBSMain()

    rospy.init_node('rbs_main')
    service_reload = rospy.Service('rbs_main_reload', ReloadList, rbs.reload_file_list)
    service_play = rospy.Service('rbs_main_play', PlaySound, rbs.play_sound)
    service_powersound = rospy.Service('rbs_main_powersound', PowerSound, rbs.powersound_state)
    service_output = rospy.Service('rbs_main_output', SelectOutput, rbs.select_output)

    rospy.spin()


if __name__ == '__main__':
    work_dir = rospy.get_param('work_dir')
    rospy.set_param('rbs_loop_data_list', dict())
    try:
        rbs_main()
    except rospy.ROSInterruptException:
        pass
