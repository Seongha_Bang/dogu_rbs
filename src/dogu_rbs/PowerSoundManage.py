import os
import sys
import shutil

import FileData as filedata


class PowerSoundManage:
    script_type = '.sh'

    def __init__(self, working_dir, manage_str, startup_file_name, shutdown_file_name):
        self.dir = working_dir
        self.m_str = manage_str

        self.stname = startup_file_name
        self.sdname = shutdown_file_name

    def CheckPlaysoundModule(self):
        return os.path.isfile(self.dir + 'playsound.py')

    def CheckStartupFile(self):
        filename = self.m_str + self.stname

        srv = os.path.isfile('/etc/systemd/system/{}.service'.format(filename))
        scr = os.path.isfile(self.dir + filename + self.script_type)

        return srv and scr

    def CheckShutdownFile(self):
        filename = self.m_str + self.sdname

        srv = os.path.isfile('/etc/systemd/system/{}.service'.format(filename))
        scr = os.path.isfile(self.dir + filename + self.script_type)

        return srv and scr

    def CreatePlaysoundModule(self):
        with open(self.dir + 'playsound.py', 'w') as f:
            f.write(filedata.playsound())
        os.system('python -m py_compile {}playsound.py'.format(self.dir))

    def CreateStartupService(self, audio_dir, plug):
        with open(self.dir + self.m_str + self.stname + self.script_type, 'w') as f:
            f.write(filedata.script(audio_dir, plug))

        os.chmod(self.dir + self.m_str + self.stname + self.script_type, 0755)

        with open(self.dir + self.m_str + 'tmp.service', 'w') as f:
            f.write(filedata.startup_srv(self.dir, self.m_str, self.stname, self.script_type))

        os.chmod(self.dir + self.m_str + 'tmp.service', 0755)
        os.system('sudo mv {}{}tmp.service /etc/systemd/system/{}{}.service'.format(self.dir, self.m_str, self.m_str,
                                                                                    self.stname))

    def CreateShutdownService(self, audio_dir, plug):
        with open(self.dir + self.m_str + self.sdname + self.script_type, 'w') as f:
            f.write(filedata.script(audio_dir, plug))

        os.chmod(self.dir + self.m_str + self.sdname + self.script_type, 0755)

        with open(self.dir + self.m_str + 'tmp.service', 'w') as f:
            f.write(filedata.shutdown_srv(self.dir, self.m_str, self.sdname, self.script_type))

        os.chmod(self.dir + self.m_str + 'tmp.service', 0755)
        os.system('sudo mv {}{}tmp.service /etc/systemd/system/{}{}.service'.format(self.dir, self.m_str, self.m_str,
                                                                                    self.sdname))

    def ChangeStartupServiceStatus(self, state):
        os.system('sudo systemctl {} {}{}.service'.format(state, self.m_str, self.stname))

    def ChangeShutdownServiceStatus(self, state):
        os.system('sudo systemctl {} {}{}.service'.format(state, self.m_str, self.sdname))

    def RemovePlaysoundModule(self):
        os.system('sudo rm {}playsound.py'.format(self.dir))
        os.system('sudo rm {}playsound.pyc 2> /dev/null'.format(self.dir))

    def RemoveStartupService(self):
        os.system('sudo rm {}{}{}{} 2> /dev/null'.format(self.dir, self.m_str, self.stname, self.script_type))
        os.system('sudo rm /etc/systemd/system/{}{}.service 2> /dev/null'.format(self.m_str, self.stname))

    def RemoveShutdownService(self):
        os.system('sudo rm {}{}{}{} 2> /dev/null'.format(self.dir, self.m_str, self.sdname, self.script_type))
        os.system('sudo rm /etc/systemd/system/{}{}.service 2> /dev/null'.format(self.m_str, self.sdname))
