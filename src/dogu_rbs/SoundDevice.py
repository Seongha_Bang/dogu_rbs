from os import popen
import re
import yaml


class SoundDevice:
    def __init__(self, work_dir):
        self.UpdateDeviceList(True)
        self.GetConfigFile(work_dir)

        if not 'select' in self.config.keys():
            self.UpdateConfigFile(work_dir, '')

    def GetConfigFile(self, work_dir):
        try:
            with open(work_dir + 'sound/config.yaml', 'r') as f:
                try:
                    self.config = yaml.full_load(f)
                except AttributeError:
                    self.config = yaml.load(f)
        except IOError:
            print('Can\'t find config file. Create a new file.')
            self.config = {}
            self.UpdateConfigFile(work_dir, '')

    def UpdateConfigFile(self, work_dir, device_name):
        self.config['list'] = self.device
        device = self.GetDevicePlug(device_name);

        if device[0]:
            self.config['select'] = device[1]
        else:
            self.config['select'] = self.device[self.device.keys()[0]]

        with open(work_dir + 'sound/config.yaml', 'w') as f:
            yaml.dump(self.config, f)

    def UpdateDeviceList(self, reset=False):
        if reset: self.device = dict()

        cmdline = popen('aplay -L').read()
        aplay = re.findall("^(plughw:.*)$\n^.*, (.*)$", cmdline, re.MULTILINE)

        for i in range(len(aplay)):
            self.device[aplay[i][1]] = aplay[i][0]

    def GetDevicePlug(self, device_name):
        device = 0
        for key in self.device.keys():
            if device_name.upper() in key.upper():
                if device == 0:
                    device = self.device[key]
                else:
                    return (False, '[Error] Duplicate value({}) - {}'.format(device_name, self.device.keys()))

        if device == 0:
            return (False, 'Value({}) not found - {}'.format(device_name, self.device.keys()))

        return (True, device)
